<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome() {
        return view('welcome');
    }
    public function register() {
        return view('register');
    }
    public function handlePost(Request $request){
        $first = $request["first"];
        $last = $request["last"];
        $nama = "$first $last";
        return view('welcome', compact('nama'));    
    }
}
